﻿using UnityEngine;

public class ChangeCursor : MonoBehaviour
{
    public Texture2D defaultCursor, hammerCursor, mopCursor;

    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    [SerializeField] private static string cursorType = "default";

    public void ChangeCursorSprite(string image)
    {
        switch (image)
        {
            case "hammer":
                Cursor.SetCursor(hammerCursor, hotSpot, cursorMode);
                cursorType = "hammer";
                Debug.Log("Pressed hammer");
                break;
            case "mop":
                Cursor.SetCursor(mopCursor, hotSpot, cursorMode);
                cursorType = "mop";
                break;
            default:
                Cursor.SetCursor(defaultCursor, hotSpot, cursorMode);
                cursorType = "default";
                break;
        }
    }

    public static string GetCursorType()
    {
        return cursorType;
    }
}
