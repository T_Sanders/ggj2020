﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ScoreController : MonoBehaviour
{
    public Text scoreText;
    public Text remainingCarsText;
    public Canvas endCanvas;
    public Text endScore;

    private int score = 0;
    private int remainingCars = 0;

    private void Start()
    {
        remainingCars = GameObject.FindGameObjectsWithTag("Racer").Length;
        scoreText.text = "Score: " + score;
        remainingCarsText.text = "Cars Remaining: " + remainingCars;
    }

    private void Update()
    {
        scoreText.text = "Score: " + score;
        remainingCarsText.text = "Cars Remaining: " + remainingCars;
    }

    public void UpdateScore()
    {
        score++;
    }

    public void UpdateCars()
    {
        remainingCars--;
        Debug.Log($"Remaining Cars: {remainingCars}");
        if(remainingCars == 0){
            DisplayEndScene();
        }
    }

    public void DisplayEndScene()
    {
        Debug.Log("END");
        endCanvas.GetComponent<Canvas>().enabled = true;
        endScore.text = "Your score: " + score;
    }
}
