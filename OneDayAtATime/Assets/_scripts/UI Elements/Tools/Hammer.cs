﻿using UnityEngine;
using UnityEngine.UI;

public class Hammer : MonoBehaviour
{
    Color _color = Color.black;

    public void OnClick()
    {
        GetComponent<Image>().color = _color;
    }
}
