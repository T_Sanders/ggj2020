﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossFinishLine : MonoBehaviour
{
    ScoreController scoreController;

    private void OnValidate()
    {
        scoreController = FindObjectOfType<ScoreController>();
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Car"))
        {
            scoreController.UpdateScore();
        }
    }
}
