﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stands : MonoBehaviour
{
    public bool isPassing;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Racer")
        {
            if (isPassing) 
                anim.SetTrigger("Passing");
            else
                anim.SetTrigger("Approaching");
                
        }
    }
}
