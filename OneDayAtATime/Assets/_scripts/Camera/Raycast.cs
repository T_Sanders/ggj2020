﻿using UnityEngine;

public class Raycast : MonoBehaviour
{
    public Camera camera;

    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            if(ChangeCursor.GetCursorType() == "hammer" && objectHit.CompareTag("Breakage"))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    TrackBehaviour trackBehaviour = hit.transform.parent.GetComponent<TrackBehaviour>();
                    trackBehaviour.SetIsDamaged(false);
                    Destroy(hit.transform.gameObject);
                }
            }

            if (ChangeCursor.GetCursorType() == "mop" && objectHit.CompareTag("Spillage"))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    TrackBehaviour trackBehaviour = hit.transform.parent.GetComponent<TrackBehaviour>();
                    trackBehaviour.SetIsDamaged(false);
                    Destroy(hit.transform.gameObject);
                }
            }
        }
    }
}
