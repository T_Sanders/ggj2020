﻿using UnityEngine;

public class CheckpointBehaviour : MonoBehaviour
{
    public int Id;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Racer") return;
        var raceCar = other.GetComponent<RaceCarBehaviour>();
        Debug.Log($"Race car {raceCar.Id} passed checkpoint {Id}.");
        raceCar.UpdateCheckpoint();
    }

}
