﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // Start is called before the first frame update
    public void OnClick(int sceneLevel)
    {
        SceneManager.LoadScene(sceneLevel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
