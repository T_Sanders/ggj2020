﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(NavMeshAgent))]
public class RaceCarBehaviour : MonoBehaviour
{
    public int Id;
    private int laps = -1;
    public int nextTargetIndex = 0;
    private List<Transform> Checkpoints = new List<Transform>();
    NavMeshAgent agent;
    ScoreController scoreController;
    public Vector3 explosionVector = new Vector3(100,5,100);
    public float TTL = 2;
    public bool die = false;
    public bool GodMode = false;
    public Animator anim;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        scoreController = GameObject.FindObjectOfType<ScoreController>();
        //anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        Checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint").OrderBy(c => {
            var cpb = c.GetComponent<CheckpointBehaviour>();
            return cpb.Id;
        }).Select(cp => cp.transform).ToList();
        Debug.Log(Checkpoints.Count);
        agent = GetComponent<NavMeshAgent>();
        agent.speed = Random.Range(15, 40);
        agent.acceleration = Random.Range(15, 40);

        Go();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (die)
        {
            TTL -= Time.deltaTime;
            if (TTL <= 0)
                agent.isStopped = true;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Breakage" || other.transform.tag == "Spillage")
            handleDestruction();

    }

    public void handleDestruction()
    {
        if (!GodMode && die != true)
        {
            anim.SetTrigger("Destroy");
            die = true;
            scoreController.UpdateCars();
        }
        //rb.isKinematic = false;
        //YEET = true;
        //Destroy(this.gameObject, 1);
    }

    public void Go(){
        Debug.Log("Go!");
        agent.destination = Checkpoints[nextTargetIndex].position;
    }

    public void UpdateCheckpoint()
    {
        nextTargetIndex++;
        
        if (nextTargetIndex == Checkpoints.Count) {
            nextTargetIndex = 0;
        }
        if(nextTargetIndex == 1){
            laps++;
            if(laps > 0)
            {
                Debug.Log($"Car {Id} just completed a lap!");
                scoreController.UpdateScore();
            }

        }
        agent.destination = Checkpoints[nextTargetIndex].position;
    }
}
