using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterMotor : MonoBehaviour {

	public float speed;
	public float gravity = 20.0f;
    public float rotationSpeed = 280;
	public Vector3 moveDirection = Vector3.zero;
    public Vector3 rotation;

	void FixedUpdate () {
		CharacterController controller;
		controller = GetComponent<CharacterController>();
		if (controller.isGrounded){
            rotation = new Vector3(0, Input.GetAxisRaw("Horizontal") * rotationSpeed * Time.deltaTime, 0);

            moveDirection = new Vector3(0, 0, Input.GetAxis ("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
		}

		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move (moveDirection * Time.deltaTime);
        this.transform.Rotate(this.rotation);
    }
}
