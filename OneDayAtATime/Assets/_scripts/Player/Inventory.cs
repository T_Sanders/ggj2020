﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> items { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        items = new List<Item>();
    }

    public void AddItem(Item item){
        Debug.Log($"Added {item.Name} to player inventory.");
        items.Add(item);
    }

    public void RemoveItem(Item item){
        items.Remove(item);
    }
}
