﻿using UnityEngine;
using System.Linq;

public class TrackController : MonoBehaviour
{
    [SerializeField]
    private TrackBehaviour[] trackTiles;

    private void OnValidate()
    {
        trackTiles = FindObjectsOfType<TrackBehaviour>().OrderBy(t => t.Id).ToArray();
    }

    public void SetTrackCondition(bool _isDamaged, int trackTile)
    {
        trackTiles[trackTile].SetIsDamaged(_isDamaged);
    }

    public bool GetTrackCondition(int trackTile)
    {
        return trackTiles[trackTile].GetIsDamaged();
    }

    public Vector3 GetTrackPosition(int trackTile)
    {
        return trackTiles[trackTile].transform.position;
    }

    public int TrackLength()
    {
        return trackTiles.Length;
    }

    public Transform GetTileTransform(int trackTile)
    {
        return trackTiles[trackTile].transform;
    }

    public bool IsTileOccupied(int trackIndex){
        if(trackIndex < 0) return false;
        return trackTiles[trackIndex].occupied;
    }
}