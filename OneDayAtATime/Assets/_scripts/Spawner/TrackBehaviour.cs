﻿using UnityEngine;

[SelectionBaseAttribute]
public class TrackBehaviour : MonoBehaviour
{
    public bool isDamaged;
    public int Id;
    public bool occupied;

    private void Start()
    {
        SetIsDamaged(false);
    }
    public bool GetIsDamaged()
    {
        return isDamaged;
    }

    public void SetIsDamaged(bool value)
    {
        isDamaged = value;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Racer") return;
        Debug.Log("On Trigger Enter");
        occupied = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag != "Racer") return;
        
        Debug.Log("On Trigger Exit");
        //This will cause a bug where if two cars are in the same tile, when the first one leaves it will mark the tile as empty but it would still be occupied.
        occupied = false;
    }
}
