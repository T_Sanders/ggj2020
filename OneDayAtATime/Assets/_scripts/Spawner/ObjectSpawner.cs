﻿using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] ObjectToSpawn;
    public TrackController trackController;
    public float RateOfSpawn = 1;
    public int spawnBuffer = 3;

    private float nextSpawn = 0;

    private void OnValidate()
    {
        trackController = FindObjectOfType<TrackController>();
    }

    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + RateOfSpawn;

            int randomTile;
            int randomObj;
            randomTile = Random.Range(0, trackController.TrackLength());
            randomObj = Random.Range(0, ObjectToSpawn.Length);
            if(randomTile < 5) return; 
            if (!trackController.GetTrackCondition(randomTile) && !CheckBufferForOccupation(randomTile))
            {
                //Spawn
                var obj = Instantiate(ObjectToSpawn[randomObj], trackController.GetTrackPosition(randomTile), transform.rotation);
                obj.transform.SetParent(trackController.GetTileTransform(randomTile));
                trackController.SetTrackCondition(true, randomTile);
            }
        }
    }

    bool CheckBufferForOccupation(int tileIndex){
        
        for(int i = 0; i < spawnBuffer; i++){
            if(trackController.IsTileOccupied(tileIndex - i)){
                return true;
            }
        }
        return false;
    }
}
