﻿using UnityEngine;

public class EyeLidBehaviour : MonoBehaviour
{
    [SerializeField]
    public bool EyesOpening;
    [SerializeField]
    public Animator Anim;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frames
    void FixedUpdate()
    {
        if (EyesOpening)
            Anim.SetFloat("speed", 1f);
        else
            Anim.SetFloat("speed", -1.0f);
    }
}
